﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classi
{
    class SnowWhite
    {
        #region Константы
        private const int SCREEN_WIDTH = 79;
        private const int SCREEN_HEIGHT = 24;
        private const int MAX_WEIGHT = 3;
        private static Random rnd = new Random();
        #endregion

        #region Поля
        private char _look;
        private ConsoleColor _color;
        private int _x, _y;
        private int _weight;
        #endregion

        #region Конструкторы
        public SnowWhite()
        {
            _look = '*';
            _color = ConsoleColor.White;
            _x = _y = 0;
            _weight = 1;
        }

        public SnowWhite(char look, ConsoleColor color = ConsoleColor.White, int weight = 1, int x = 0, int y = 0)
        {
            _look = look;
            _color = color;
            _x = x;
            _y = y;
            _weight = weight;
        }

        public SnowWhite(SnowWhite sw)
        {
            _look = sw._look;
            _color = sw._color;
            _x = sw._x;
            _y = sw._y;
            _weight = sw._weight;
        }
        #endregion

        #region Сеттеры
        public void Setlook(char look)
        {
            _look = look;
        }

        public void Setcolor(ConsoleColor color)
        {
            _color = color;
        }

        public void SetXY(int x, int y)
        {
            if (x >= 0 && x <= SCREEN_WIDTH &&
                y >= 0 && y <= SCREEN_HEIGHT)
            {
                _x = x;
                _y = y;
            }
        }

        public void SetWeight(int weight)
        {
            if (weight >= 1 && weight <= MAX_WEIGHT)
            {
                _weight = weight;
            }
        }
        #endregion

        #region Геттеры
        public char GetLook()
        {
            return _look;
        }

        public ConsoleColor GetColor()
        {
            return _color;
        }

        public int GetX()
        {
            return _x;
        }

        public int GetY()
        {
            return _y;
        }

        public int GetWeight()
        {
            return _weight;
        }
        #endregion

        #region Методы
        /// <summary>
        /// Рандомирует x,y, цвет и вес снежинки
        /// </summary>
        public void SetRandomParameters()
        {
            _x = rnd.Next(0, SCREEN_WIDTH);
            _y = rnd.Next(0, SCREEN_HEIGHT);
            _weight = rnd.Next(1, MAX_WEIGHT + 1);
            _color = (ConsoleColor)rnd.Next(1, 15 + 1);
        }

        public void Show()
        {
            Console.ForegroundColor = _color;
            Console.SetCursorPosition(_x, _y);
            Console.Write(_look);
        }

        public void FallDown()
        {
            _y += _weight;
            if(_y > SCREEN_HEIGHT)
            {
                {
                    _y = 0;
                }
            }
        }
        #endregion
    }
}
