﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classi
{
    class SnowFall
    {
        private SnowWhite[] snowwhites;

        private void PlaceSnowWight()
        {
            for (int i = 0; i < snowwhites.Length; i++)
            {
                snowwhites[i] = new SnowWhite();
                snowwhites[i].SetRandomParameters();
            }
        }

        private void MakeSnowFallStep()
        {
            for (int i = 0; i < snowwhites.Length; i++)
            {
                snowwhites[i].FallDown();                
                snowwhites[i].Show();         
            }
        }

        public SnowFall(int count=10)
        {
            snowwhites = new SnowWhite[count];
            PlaceSnowWight();
        }

        /// <summary>
        /// Запуск бесконечго снегопада
        /// </summary>
        /// <param name="delay">Задержка в милисекундах</param>
        public void StartSnowFall(int delay = 1000)
        {
            while(true)
            {
                Console.Clear();
                MakeSnowFallStep();
                System.Threading.Thread.Sleep(delay);
            }
        }
    }
}
